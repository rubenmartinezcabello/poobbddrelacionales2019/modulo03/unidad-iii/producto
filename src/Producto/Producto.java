package Producto;

/**
 *
 * @author Rubén Martínez Cabello 71421914-Z
 *
**/

public class Producto 
{
    public int codigo;
    public String descripcion;
    public double precio;
    
    public Producto( int codigo, String descripcion, double precio )
    {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio;
    }
    
    public void fijarPrecio( double precio )
    {
        this.precio = precio;
    }
    public double obtenerPrecio( )
    {
        return this.precio;
    }
    
    public static void main( String args[] )
    {
        Producto sal = new Producto( 80005355, "Sal", 0.60 );
        Producto azucar = new Producto( 80005388, "Azucar", 0.81);
        
        System.out.println( "Precio del paquete de "+ sal.descripcion + ": " + sal.obtenerPrecio() + "€" );
        System.out.println( "Precio del paquete de "+ azucar.descripcion + ": " + azucar.obtenerPrecio() + "€" );
        
    }
    
}
